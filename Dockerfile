# cat Dockerfile
FROM mysql:latest


RUN chown -R mysql:root /var/lib/mysql/

ADD mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf

ADD init.sql /etc/mysql/init.sql

ENV MYSQL_DATABASE=cookfreshmain \
    MYSQL_ROOT_PASSWORD=test123 \
    MYSQL_USER=shoot \
    MYSQL_PASSWORD=shoot \
    MYSQL_RANDOM_ROOT_PASSWORD=no \
    MYSQL_ONETIME_PASSWORD=no

COPY init.sql /docker-entrypoint-initdb.d/

EXPOSE 3306



# FROM ubuntu/mysql:latest

# RUN apt-get update 
# RUN apt-get install -y curl git wget nano net-tools inetutils-ping unzip libgconf-2-4 gdb libstdc++6 libglu1-mesa fonts-droid-fallback lib32stdc++6 python3 psmisc
# RUN apt-get clean

# 

# ARG MYSQL_DATABASE
# ARG MYSQL_USER
# ARG MYSQL_PASSWORD
# ARG MYSQL_ROOT_PASSWORD
# ARG MYSQL_RANDOM_ROOT_PASSWORD
# ARG MYSQL_ONETIME_PASSWORD
# ARG DOCKER_ENTRYPOINT_CMD

# ENV MYSQL_DATABASE=$MYSQL_DATABASE
# ENV MYSQL_USER=$MYSQL_USER
# ENV MYSQL_PASSWORD=$MYSQL_PASSWORD
# ENV MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD
# ENV MYSQL_RANDOM_ROOT_PASSWORD=${MYSQL_RANDOM_ROOT_PASSWORD}
# ENV MYSQL_ONETIME_PASSWORD=${MYSQL_ONETIME_PASSWORD}

# ENV DOCKER_ENTRYPOINT_CMD=${DOCKER_ENTRYPOINT_CMD}




# RUN sed -i 's/MYSQL_DATABASE/'$MYSQL_DATABASE'/g' /etc/mysql/init.sql
# RUN cp /etc/mysql/init.sql /docker-entrypoint-initdb.d

# RUN mysqld_safe --initialize --init-file=/etc/mysql/init.sql

# ENTRYPOINT $DOCKER_ENTRYPOINT_CMD

# EXPOSE 3306